#ifndef SPIFFSutils_h
    #define SPIFFSutils_h


    #if ARDUINO >= 100
        #include "Arduino.h"

        // include any supporting libraries here
        #include "FS.h"
        #include "SPIFFS.h"

    #else
        #include "WProgram.h"
        #include "pins_arduino.h"
        #include "WConstants.h"
    #endif


    class SPIFFSutils {

        public:
            SPIFFSutils(); // constructor

            String readFile(const char* type, const char* path);
            String writeFile(const char* type, const char* path, const char* content);
            String deleteFile(const char* path);
            void ls();

        private:
            String _init();
            File _file;
            File _root;
            const char* _s = "s"; // "s" for String SPIFFS content
    };
#endif
