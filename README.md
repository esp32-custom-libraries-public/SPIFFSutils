# SPIFFSutils

A custom ESP32 library for reading, writing, appending, and listing text files in the SPI flash file system (SPIFFS).

## Notes
- This version removes the appendToFile() method from the previous version because it does not produce the expected result (appends content more than once instead of just one time)
- The first time you run this sketch, or any time after you've cleared the ESP32 flash memory, you will see this error: "E (14) SPIFFS: mount failed, -10025". it should resolve on its own with no intervention after about 30 seconds
- Calls to SPIFFSutils methods expect "s" as the first argument to signify a String because ostensibly it should be possible to read and write other file types (i.e., images) which would need to be handled differently
- To erase the flash memory (both SPIFFS and EEPROM simultaneously), in PlatformIO sidebar select Project Tasks > Platform > Erase Flash
- Writing to SPIFFS is limited to 10,000 instances
