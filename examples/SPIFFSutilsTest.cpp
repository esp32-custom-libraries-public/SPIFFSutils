#include <Arduino.h>
#include <SPIFFSutils.h>


SPIFFSutils mySPIFFSutils = SPIFFSutils();

String fileContents_read;
const char* fileContents_toWrite = "Go ahead, make my day.";


void setup() {
    Serial.begin(9600);

    Serial.println("\n\nStarting SPIFFS test:");
    Serial.println("---------------------");

    // ** list existing files test 1 **
    // files are printed to serial monitor from library
    // no value is returned (void)
    Serial.println("\nlist files with ls():");
    mySPIFFSutils.ls();

    // -----------------------------

    delay(1000);

    // ** read file test - non-existent text file **
    // assumes no pre-existing files in SPIFFS memory.
    // the "s" is for SPIFFS content of type String.
    // expects a String variable to store return value.
    // expected result: "Specified file does not exist"
    Serial.println("\nreadFile(): /non_existent.txt");
    fileContents_read = mySPIFFSutils.readFile("s", "/non_existent.txt");
    Serial.println(fileContents_read);

    // -----------------------------

    delay(1000);

    // ** write text file test **
    // if the file doesn't exist, it will be created.
    // if the file already exists, it will be overwritten
    Serial.print("\nwriteFile(): /movie_quotes.txt: ");
    mySPIFFSutils.writeFile("s", "/movie_quotes.txt", fileContents_toWrite);
    delay(1000);
    Serial.println("done");

    // -----------------------------

    delay(1000);

    // ** read file test - previously created text file **
    // the "s" is for SPIFFS content of type String.
    // expects a String variable to store return value.
    // expected result: "Go ahead, make my day."
    Serial.println("\nreadFile(): /movie_quotes.txt");
    fileContents_read = mySPIFFSutils.readFile("s", "/movie_quotes.txt");
    Serial.println(fileContents_read);

    // -----------------------------

    delay(1000);

    // ** delete file test **
    Serial.print("\ndeleteFile(): /movie_quotes.txt: ");
    mySPIFFSutils.deleteFile("/movie_quotes.txt");
    delay(1000);
    Serial.println("done");

    // -----------------------------

    delay(1000);

    // ** list existing files test 2 **
    // files are printed to serial monitor from library
    // no value is returned (void)
    Serial.println("\nlist files with ls():");
    mySPIFFSutils.ls();

    // -----------------------------

    delay(1000);

    Serial.println("\nSPIFFS test done.");
    Serial.println("-----------------");
}


void loop() {}
