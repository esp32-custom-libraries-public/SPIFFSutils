#include "SPIFFSutils.h"


SPIFFSutils::SPIFFSutils() {} // constructor


String SPIFFSutils::_init() {
    if (!SPIFFS.begin(true)) return "An Error has occurred while mounting SPIFFS";
    return "Success SPIFFS mount";
}


String SPIFFSutils::readFile(
    const char *type,
    const char *path) {
    _init();

    String _str = "";
    _file = SPIFFS.open(path, FILE_READ);
    if (!_file) return "Failed opening file";

    while (_file.available()) {
        if (type == _s) _str += char(_file.read());
    }
    _file.close();

    if (_str.length() > 0) return _str;
    return "Specified file does not exist";
}


String SPIFFSutils::writeFile(
    const char *type,
    const char *path,
    const char *content) {
    _init();

    _file = SPIFFS.open(path, FILE_WRITE);
    if (!_file) return "Failed opening file";

    if (type == _s) {
        if (!_file.print(content)) return "Failed writing file";
    }
    _file.close();
    return "Success writing file";
}


String SPIFFSutils::deleteFile(
    const char *path) {

    if (SPIFFS.remove(path)){
        return "Failed deletion";
    } else {
        return "Success deletion";
    }
}


void SPIFFSutils::ls() {
  _init();

  _root = SPIFFS.open("/");
  _file = _root.openNextFile();

  while(_file) {
    Serial.print(_file.name());
    Serial.print("\t");
    Serial.print(_file.size());
    Serial.println("KB");

    _file = _root.openNextFile();
  }
  _file.close();
}
